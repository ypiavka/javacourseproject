package org.training.center.program;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.training.center.program.entity.Course;
import org.training.center.program.repository.CourseRepository;

import java.time.Duration;

public class CourseRepositoryTest {

    @Test
    public void testGetCourseByName() {
        Course course = CourseRepository.getCourseByName("Java");

        String expectedCourseName = "Java";
        Duration expectedCourseDuration = Duration.ofHours(16);

        String receivedCourseName = course.getCourseName();
        Duration receivedCourseDuration = course.getDuration();

        Assertions.assertEquals(expectedCourseDuration, receivedCourseDuration);
        Assertions.assertEquals(expectedCourseName, receivedCourseName);
    }
}