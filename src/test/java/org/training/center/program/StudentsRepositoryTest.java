package org.training.center.program;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.training.center.program.entity.Student;
import org.training.center.program.repository.StudentsRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;

public class StudentsRepositoryTest {
    @Test
    public void testGetStudentByName() {
        String expectedStudentName = "Test Student";
        String expectedProgramName = "Java Developer";
        LocalDateTime expectedProgramStartDate = LocalDateTime.of(
              LocalDate.of(2023, Month.SEPTEMBER, 21),
              LocalTime.of(10, 0));

        Student student = StudentsRepository.getStudentByName("Test Student");
        String receivedStudentName = student.getName();
        String receivedProgramName = student.getProgramName();
        LocalDateTime receivedProgramStartDate = student.getProgramStartDate();

        Assertions.assertEquals(expectedStudentName, receivedStudentName);
        Assertions.assertEquals(expectedProgramName, receivedProgramName);
        Assertions.assertEquals(expectedProgramStartDate, receivedProgramStartDate);
    }
}