package org.training.center.program;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.training.center.program.entity.Student;
import org.training.center.program.repository.StudentsRepository;
import org.training.center.program.util.ReportDateUtils;

import java.time.*;

public class ReportBuilderUtilsTest {
    public static final Student STUDENT = StudentsRepository.getStudentByName("Test Student");

    @ParameterizedTest
    @CsvSource(value = {"2023-09-20T18:00, true", "2023-10-03T13:30, false", "2023-09-21T10:01, false"})
    public void testHasTheStudentStartedTheProgram(LocalDateTime reportLaunchDate, boolean expectedResult) {
        boolean receivedResult = ReportDateUtils.hasTheStudentStartedTheProgram(STUDENT, reportLaunchDate);

        Assertions.assertEquals(expectedResult, receivedResult);
    }

    @Test
    public void testDateTimeFormatterateTimeFormatter() {
        String expectedDate = "10:00, Thursday, 21 September 2023";
        String receivedDate = ReportDateUtils.dateTimeFormatter(LocalDateTime.of(
              LocalDate.of(2023, Month.SEPTEMBER, 21),
              LocalTime.of(10, 0)));

        Assertions.assertEquals(expectedDate, receivedDate);
    }

    @Test
    public void testFormatDuration() {
        String expectedDuration = "2 days, 8 hours";
        String receivedDuration = ReportDateUtils.formatDuration(Duration.ofHours(56));

        Assertions.assertEquals(expectedDuration, receivedDuration);
    }

    @Test
    public void testIsDateValidTrue() {
        Assertions.assertTrue(ReportDateUtils.isValidDate("2023-10-03 13:30"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"", "  ", "2023-09-20, 18:00", "2023/09/21 10:01"})
    public void testIsDateValidFalse(String date) {
        Assertions.assertFalse(ReportDateUtils.isValidDate(date));
    }
}