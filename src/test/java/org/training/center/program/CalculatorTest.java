package org.training.center.program;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.training.center.program.entity.Student;
import org.training.center.program.repository.StudentsRepository;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Month;

public class CalculatorTest {
    public static final Student STUDENT = StudentsRepository.getStudentByName("Test Student");

    @Test
    public void testWhenTheProgramToBeCompleted() {
        LocalDateTime expectedDate = LocalDateTime.of(2023, Month.SEPTEMBER, 29, 18, 0);
        LocalDateTime receivedDate = Calculator.whenTheProgramToBeCompleted(STUDENT);
        LocalDateTime programStartDate = STUDENT.getProgramStartDate();

        Assertions.assertEquals(expectedDate, receivedDate);
        Assertions.assertTrue(receivedDate.isAfter(programStartDate));
    }

    @Test
    public void testGetProgramDuration() {
        Duration expectedDuration = Duration.ofHours(56);
        Duration receivedDuration = Calculator.getProgramDuration(STUDENT);

        Assertions.assertEquals(expectedDuration, receivedDuration);
        Assertions.assertFalse(receivedDuration.isZero());
        Assertions.assertFalse(receivedDuration.isNegative());
    }

    @ParameterizedTest
    @CsvSource(value = {"2023-09-30T18:00, PT-24H", "2023-10-03T13:30, PT-91H-30M", "2024-09-30T18:00, PT-8808H"})
    public void testDurationBetweenProgramFinishDateAndReportLaunchDateWhenTheProgramWasFinishedBeforeLaunchDate(LocalDateTime reportLaunchDate
          , Duration expectedDurationSinceProgramWasFinished) {
        Duration receivedDurationSinceProgramWasFinished = Calculator.durationBetweenProgramFinishDateAndReportLaunchDate(STUDENT, reportLaunchDate);

        Assertions.assertEquals(expectedDurationSinceProgramWasFinished, receivedDurationSinceProgramWasFinished);
        Assertions.assertTrue(receivedDurationSinceProgramWasFinished.isNegative());
        Assertions.assertFalse(receivedDurationSinceProgramWasFinished.isZero());
    }

    @ParameterizedTest
    @CsvSource(value = {"2023-09-21T18:00, PT144H", "2023-09-29T10:00, PT24H", "2023-09-25T13:30, PT100H30M"})
    public void testDurationBetweenProgramFinishDateAndReportLaunchDateWhenTheProgramWasNotFinishedYet(LocalDateTime reportLaunchDate
          , Duration expectedDurationSinceProgramWasFinished) {
        Duration receivedDurationSinceProgramWasFinished = Calculator.durationBetweenProgramFinishDateAndReportLaunchDate(STUDENT, reportLaunchDate);

        Assertions.assertEquals(expectedDurationSinceProgramWasFinished, receivedDurationSinceProgramWasFinished);
        Assertions.assertFalse(receivedDurationSinceProgramWasFinished.isNegative());
        Assertions.assertFalse(receivedDurationSinceProgramWasFinished.isZero());
    }

    @Test
    public void testDurationBetweenProgramFinishDateAndReportLaunchDateIfLaunchDateIsTheSameAsFinishDate() {
        Duration expectedDurationSinceProgramWasFinished = Duration.ZERO;
        Duration receivedDurationSinceProgramWasFinished = Calculator.durationBetweenProgramFinishDateAndReportLaunchDate(STUDENT,
              LocalDateTime.of(2023, Month.SEPTEMBER, 29, 18, 00));

        Assertions.assertEquals(expectedDurationSinceProgramWasFinished, receivedDurationSinceProgramWasFinished);
        Assertions.assertFalse(receivedDurationSinceProgramWasFinished.isNegative());
        Assertions.assertTrue(receivedDurationSinceProgramWasFinished.isZero());
    }

}
