package org.training.center.program.util;

import org.training.center.program.entity.Student;

import java.sql.Date;
import java.text.DateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReportDateUtils {
    public static final Locale LOCALE = new Locale("en", "GB");
    public static final DateFormat DATE_FORMAT = DateFormat.getDateInstance(DateFormat.FULL, LOCALE);
    public static final String DATE_CHECK_REGEX = "^\\d{4}-\\d{2}-\\d{2}\\s\\d{2}:\\d{2}$";

    public static boolean hasTheStudentStartedTheProgram(Student student, LocalDateTime dateForTheReportGeneration) {
        return student.getProgramStartDate().isAfter(dateForTheReportGeneration);
    }

    public static String dateTimeFormatter(LocalDateTime dateToFormat) {
        String date = DATE_FORMAT.format(Date.valueOf(dateToFormat.toLocalDate()));
        String time = dateToFormat.toLocalTime().truncatedTo(ChronoUnit.MINUTES).toString();
        return time + ", " + date;
    }

    public static String formatDuration(Duration duration) {
        List<String> parts = new ArrayList<>();
        long days = duration.toDaysPart();
        if (days != 0) {
            parts.add(convertToPlural(Math.abs(days), "day"));
        }
        long hours = duration.toHoursPart();
        if (hours != 0) {
            parts.add(convertToPlural(Math.abs(hours), "hour"));
        }
        long minutes = duration.toMinutesPart();
        if (minutes != 0) {
            parts.add(convertToPlural(Math.abs(minutes), "minute"));
        }
        return String.join(", ", parts);
    }

    public static String convertToPlural(long num, String unit) {
        return num + " " + unit + (num == 1 ? "" : "s");
    }

    public static boolean isValidDate(String date) {
        if (date == null) {
            return false;
        }
        Matcher m = Pattern.compile(DATE_CHECK_REGEX).matcher(date);
        return m.matches();
    }

}

