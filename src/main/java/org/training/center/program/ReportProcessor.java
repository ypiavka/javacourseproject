package org.training.center.program;

import org.training.center.program.entity.Student;
import org.training.center.program.repository.StudentsRepository;
import org.training.center.program.util.ReportDateUtils;

import java.sql.Date;
import java.text.DateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Locale;
import java.util.Scanner;

public class ReportProcessor {
    public static final Locale LOCALE = new Locale("en", "GB");
    public static final DateFormat DATE_FORMAT = DateFormat.getDateInstance(DateFormat.FULL, LOCALE);
    public static final String FULL_REPORT_TYPE = "full";
    public static final String SHORT_REPORT_TYPE = "short";

    public static void generateReport() {
        Scanner userInput = new Scanner(System.in);

        System.out.print("Type in a date for which you would like to generate a report (YYYY-MM-DD HH:MM): ");
        String userDate = userInput.nextLine();
        while (!ReportDateUtils.isValidDate(userDate)) {
            System.err.println("Please enter the date for report calculation using pattern YYYY-MM-DD HH:MM : ");
            userDate = userInput.nextLine();
        }

        System.out.print("Type in a name of a student (e.g. Ivan Ivanov): ");
        String studentName = userInput.nextLine();
        Student student = StudentsRepository.getStudentByName(studentName);
        while (student == null) {
            System.err.println("Please enter valid student name: ");
            studentName = userInput.nextLine();
        }

        System.out.print("Would you like to generate short of full report: ");
        String reportType = userInput.nextLine();
        while (!reportType.equalsIgnoreCase(FULL_REPORT_TYPE) && !reportType.equalsIgnoreCase(SHORT_REPORT_TYPE)) {
            System.err.println("Please choose valid report type (short or full):");
            reportType = userInput.nextLine();
        }

        LocalDateTime dateForTheReportGeneration = LocalDateTime.parse(userDate, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        String reportGenerationDateToBeShownInReport = DATE_FORMAT.format(Date.valueOf(dateForTheReportGeneration.toLocalDate()));
        String reportGenerationTimeToBeShownInReport = dateForTheReportGeneration.toLocalTime().truncatedTo(ChronoUnit.MINUTES).toString();
        String programTheStudentPasses = student.getProgramName();

        if (ReportDateUtils.hasTheStudentStartedTheProgram(student, dateForTheReportGeneration)) {
            System.out.println("The student hasn't started the program yet on the " + userDate);
            System.out.println("Report cannot be generated");
        } else {
            System.out.println("\nGenerating report date - " + reportGenerationTimeToBeShownInReport + ", " + reportGenerationDateToBeShownInReport);
            if (reportType.equalsIgnoreCase(FULL_REPORT_TYPE)) {
                generateFullReport(student, programTheStudentPasses);
            } else {
                System.out.println(studentName + " (" + programTheStudentPasses + ")");
            }
            Duration timeToTheProgramFinish = Calculator.durationBetweenProgramFinishDateAndReportLaunchDate(student, dateForTheReportGeneration);
            String timeToPrintInReport = ReportDateUtils.formatDuration(timeToTheProgramFinish);
            if (timeToTheProgramFinish.isNegative()) {
                System.out.println("Training completed. " + timeToPrintInReport + " have passed since the program was finished.");
            } else if (timeToTheProgramFinish.isZero()) {
                System.out.println("Training has just been completed.");
            } else {
                System.out.println("Training is not finished yet. " + timeToPrintInReport + " left until the end.");
            }
        }
    }

    private static void generateFullReport(Student student, String programTheStudentPasses) {
        long programDuration = Calculator.getProgramDuration(student).toHours();
        String programStartDateTime = ReportDateUtils.dateTimeFormatter(student.getProgramStartDate());
        String programEndDateTime = ReportDateUtils.dateTimeFormatter(Calculator.whenTheProgramToBeCompleted(student));
        System.out.println();
        System.out.println(student.getName());
        System.out.println("Working time: 10 am to 18 pm.");
        System.out.println("Program: " + programTheStudentPasses);
        System.out.println("The program duration: " + programDuration + " hours.");
        System.out.println("The program start date: " + programStartDateTime);
        System.out.println("The program end date: " + programEndDateTime);
    }
}
