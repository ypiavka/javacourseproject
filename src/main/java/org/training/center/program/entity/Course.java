package org.training.center.program.entity;

import java.time.Duration;
import java.util.Objects;

public class Course {
    private String courseName;
    private Duration duration;

    public Course(String courseName, Duration duration) {
        this.courseName = courseName;
        this.duration = duration;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Course course = (Course) o;
        return Objects.equals(courseName, course.courseName)
              && Objects.equals(duration, course.duration);
    }

    @Override
    public int hashCode() {
        return Objects.hash(courseName, duration);
    }
}
