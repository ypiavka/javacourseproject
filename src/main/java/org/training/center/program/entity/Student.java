package org.training.center.program.entity;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Objects;

public class Student {
    private String name;
    private String programName;
    private LocalDateTime programStartDate;
    private ArrayList<Course> courses;
    public Student(String name, String programName, LocalDateTime programStartDate, ArrayList<Course> courses) {
        this.name = name;
        this.programName = programName;
        this.programStartDate = programStartDate;
        this.courses = courses;
    }
    public String getName() {
        return name;
    }

    public void setName(String firstName) {
        this.name = firstName;
    }
    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }
    public ArrayList<Course> getCourses() {
        return courses;
    }

    public void setCourses(ArrayList<Course> courses) {
        this.courses = courses;
    }
    public LocalDateTime getProgramStartDate() {
        return programStartDate;
    }

    public void setProgramStartDate(LocalDateTime programStartDate) {
        this.programStartDate = programStartDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Objects.equals(name, student.name)
              && Objects.equals(programName, student.programName)
              && Objects.equals(programStartDate, student.programStartDate)
              && Objects.equals(courses, student.courses);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, programName, programStartDate, courses);
    }
}
