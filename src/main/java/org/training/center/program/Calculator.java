package org.training.center.program;

import org.training.center.program.entity.Course;
import org.training.center.program.entity.Student;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;

public class Calculator {
    public static final LocalTime START_OF_WORKING_HOURS = LocalTime.of(10, 0);
    public static final LocalTime END_OF_WORKING_HOURS = LocalTime.of(18, 0);
    public static final long WORKING_DAY_DURATION = 8;

    public static LocalDateTime whenTheProgramToBeCompleted(Student student) {
        Duration programDuration = getProgramDuration(student);

        LocalDateTime programStartDate = student.getProgramStartDate();
        programStartDate = weekendDaysSkip(programStartDate);
        programStartDate = setTheTimeWithinLearningHours(programStartDate);

        Duration hoursToBePassedDuringFirstDay = Duration.between(programStartDate.toLocalTime(), END_OF_WORKING_HOURS);
        Duration learningHoursLeft = programDuration.minus(hoursToBePassedDuringFirstDay);

        if (learningHoursLeft.isZero() || learningHoursLeft.isNegative()) {
            return programStartDate.plusHours(programDuration.toHours());
        }
        return calculateProgramEndDateIfItTakesMoreThanOneDay(programStartDate, learningHoursLeft);
    }

    public static Duration durationBetweenProgramFinishDateAndReportLaunchDate(Student student, LocalDateTime reportLaunchDate) {
        LocalDateTime dateTheProgramToBeFinished = whenTheProgramToBeCompleted(student);
        if (reportLaunchDate.isBefore(dateTheProgramToBeFinished)) {
            return howMuchTimeLeftBeforeTheProgramToBeFinished(student, reportLaunchDate);
        }
        return Duration.between(reportLaunchDate, dateTheProgramToBeFinished);
    }

    private static Duration howMuchTimeLeftBeforeTheProgramToBeFinished(Student student, LocalDateTime reportLaunchDate) {
        Duration learningHoursLeft = getProgramDuration(student);
        LocalDateTime programStartDate = student.getProgramStartDate();
        programStartDate = weekendDaysSkip(programStartDate);
        programStartDate = setTheTimeWithinLearningHours(programStartDate);

        reportLaunchDate = weekendDaysSkip(reportLaunchDate);
        reportLaunchDate = setTheTimeWithinLearningHours(reportLaunchDate);
        if (programStartDate.equals(reportLaunchDate)) {
            return durationConversionToEightHourWorkingDay(learningHoursLeft);
        }
        if (programStartDate.toLocalDate().equals(reportLaunchDate.toLocalDate())) {
            Duration hoursBetweenStarDateAndReportLaunch = Duration.between(programStartDate.toLocalTime(), reportLaunchDate.toLocalTime());
            learningHoursLeft = learningHoursLeft.minus(hoursBetweenStarDateAndReportLaunch);
            return durationConversionToEightHourWorkingDay(learningHoursLeft);
        }
        Duration hoursToBePassedDuringFirstDay = Duration.between(programStartDate.toLocalTime(), END_OF_WORKING_HOURS);
        learningHoursLeft = learningHoursLeft.minus(hoursToBePassedDuringFirstDay);
        programStartDate = incrementForADayWithWeekendDaysSkip(programStartDate);
        while (!programStartDate.toLocalDate().equals(reportLaunchDate.toLocalDate())) {
            learningHoursLeft = learningHoursLeft.minus(Duration.ofHours(WORKING_DAY_DURATION));
            programStartDate = incrementForADayWithWeekendDaysSkip(programStartDate);
        }
        learningHoursLeft = learningHoursLeft.minus(Duration.between(programStartDate.toLocalTime(), reportLaunchDate.toLocalTime()));
        return durationConversionToEightHourWorkingDay(learningHoursLeft);
    }

    private static LocalDateTime calculateProgramEndDateIfItTakesMoreThanOneDay(LocalDateTime programStartDate, Duration learningHoursLeft) {
        LocalDateTime nextDayAfterStartTime = incrementForADayWithWeekendDaysSkip(programStartDate);
        if (learningHoursLeft.toHours() <= WORKING_DAY_DURATION) {
            return nextDayAfterStartTime.plus(learningHoursLeft);
        }
        long fullLearningDaysLeft = learningHoursLeft.dividedBy(WORKING_DAY_DURATION).toHours();
        LocalDateTime lastLearningDay = nextDayAfterStartTime;
        for (long i = 1; i <= fullLearningDaysLeft; i++) {
            lastLearningDay = incrementForADayWithWeekendDaysSkip(lastLearningDay);
        }
        if (learningHoursLeft.minusHours(fullLearningDaysLeft * WORKING_DAY_DURATION).toHours() > 0) {
            return lastLearningDay.plusHours(learningHoursLeft.minusHours(fullLearningDaysLeft * WORKING_DAY_DURATION).toHours());
        }
        if ("MONDAY".equals(lastLearningDay.getDayOfWeek().toString())) {
            return lastLearningDay.minusHours(64);
        }
        return lastLearningDay.minusHours(16);
    }

    private static LocalDateTime setTheTimeWithinLearningHours(LocalDateTime timeToCheck) {
        if (timeToCheck.toLocalTime().isBefore(START_OF_WORKING_HOURS)) {
            timeToCheck = LocalDateTime.of(timeToCheck.toLocalDate(), START_OF_WORKING_HOURS);
        } else if (timeToCheck.toLocalTime().isAfter(END_OF_WORKING_HOURS)) {
            timeToCheck = incrementDateForANumberOfDays(timeToCheck, 1);
        }
        return timeToCheck;
    }

    private static LocalDateTime incrementDateForANumberOfDays(LocalDateTime dateToIncrement, long daysToAdd) {
        return LocalDateTime.of(dateToIncrement.toLocalDate().plusDays(daysToAdd), START_OF_WORKING_HOURS);
    }

    private static LocalDateTime weekendDaysSkip(LocalDateTime dateToIncrement) {
        int daysToAdd = 0;
        if ("SUNDAY".equals(dateToIncrement.getDayOfWeek().toString())) {
            daysToAdd = 1;
        } else if ("SATURDAY".equals(dateToIncrement.getDayOfWeek().toString())) {
            daysToAdd = 2;
        }
        return daysToAdd == 0 ? dateToIncrement : incrementDateForANumberOfDays(dateToIncrement, daysToAdd);
    }

    private static LocalDateTime incrementForADayWithWeekendDaysSkip(LocalDateTime dateToIncrement) {
        if ("FRIDAY".equals(dateToIncrement.getDayOfWeek().toString())) {
            return incrementDateForANumberOfDays(dateToIncrement, 3);
        }
        return incrementDateForANumberOfDays(dateToIncrement, 1);
    }

    private static Duration durationConversionToEightHourWorkingDay(Duration duration) {
        long fullDays = duration.toHours() / WORKING_DAY_DURATION;
        long leftHours = duration.toHours() - (fullDays * WORKING_DAY_DURATION);
        long leftMinutes = duration.toMinutesPart();
        return Duration.ofDays(fullDays).plusHours(leftHours).plusMinutes(leftMinutes);
    }

    public static Duration getProgramDuration(Student student) {
        ArrayList<Course> courses = student.getCourses();
        int coursesSize = student.getCourses().size();

        Duration sumOfAllCoursesDurations = Duration.ZERO;
        for (int i = (coursesSize - 1); i >= 0; i--) {
            Duration someCourseDuration = courses.get(i).getDuration();
            sumOfAllCoursesDurations = sumOfAllCoursesDurations.plus(someCourseDuration);
        }
        return sumOfAllCoursesDurations;
    }
}
