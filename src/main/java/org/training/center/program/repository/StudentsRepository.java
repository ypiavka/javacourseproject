package org.training.center.program.repository;

import org.training.center.program.entity.Student;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StudentsRepository {
    public static List<Student> getStudents() {
        ArrayList<Student> students = new ArrayList<>();
        students.add(
              new Student("Ivan Ivanov", "Java Developer",
                    LocalDateTime.of(LocalDate.of(2020, Month.JUNE, 1), LocalTime.of(10, 0)),
                    new ArrayList<>(Arrays.asList(
                          CourseRepository.getCourseByName("Java"),
                          CourseRepository.getCourseByName("JDBC"),
                          CourseRepository.getCourseByName("Spring")))
              ));
        students.add(
              new Student("Ivan Sidorov", "AQE",
                    LocalDateTime.of(LocalDate.of(2020, Month.JUNE, 1), LocalTime.of(10, 0)),
                    new ArrayList<>(Arrays.asList(
                          CourseRepository.getCourseByName("Test design"),
                          CourseRepository.getCourseByName("Page Object"),
                          CourseRepository.getCourseByName("Selenium")))
              ));
        students.add(
              new Student("Test Student", "Java Developer",
                    LocalDateTime.of(LocalDate.of(2023, Month.SEPTEMBER, 21), LocalTime.of(10, 0)),
                    new ArrayList<>(Arrays.asList(
                          CourseRepository.getCourseByName("Java"),
                          CourseRepository.getCourseByName("JDBC"),
                          CourseRepository.getCourseByName("Spring")))
              ));
        return students;
    }

    public static Student getStudentByName(String studentName) {
        List<Student> students = new ArrayList<>(StudentsRepository.getStudents());
        return students.stream()
              .filter(c -> c.getName().equals(studentName))
              .findFirst().orElse(null);
    }

}
