package org.training.center.program.repository;

import org.training.center.program.entity.Course;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class CourseRepository {
    public static ArrayList<Course> getCourses() {
        ArrayList<Course> courses = new ArrayList<>();
        courses.add(new Course("Java", Duration.ofHours(16)));
        courses.add(new Course("JDBC", Duration.ofHours(24)));
        courses.add(new Course("Spring", Duration.ofHours(16)));
        courses.add(new Course("Test design", Duration.ofHours(10)));
        courses.add(new Course("Page Object", Duration.ofHours(16)));
        courses.add(new Course("Selenium", Duration.ofHours(16)));
        return courses;
    }

    public static Course getCourseByName(String courseName) {
        List<Course> courses = new ArrayList<>(CourseRepository.getCourses());
        return courses.stream()
              .filter(c -> c.getCourseName().equals(courseName))
              .findFirst().orElse(new Course("No such course", null));
    }
}

